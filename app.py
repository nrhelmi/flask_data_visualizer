from flask import Flask, render_template,request,redirect, url_for,send_file, send_from_directory, jsonify
from flask_wtf import FlaskForm
from wtforms.validators import InputRequired
from wtforms import *
from sqlparser import *
import socket
import re
import requests
from pymongo import MongoClient
from bson.json_util import dumps
from bson.objectid import ObjectId
import time
import os
from flask_socketio import SocketIO, emit


app = Flask(__name__)
app.config['SECRET_KEY'] = 'Thisisasecret!'
socketio = SocketIO(app)

class MultiCheckboxField(SelectMultipleField):
    widget = widgets.ListWidget(prefix_label=False)
    option_widget = widgets.CheckboxInput()

class QueryForm(FlaskForm):
   ip = TextField("IP", validators=[InputRequired()])
   port = TextField("Port", validators=[InputRequired()])
   selectlist = MultiCheckboxField('', choices= [('temperature', 'Temperature'), ('humidity', 'Humidity'),('brightness','Brightness')])
   attribute = SelectField('', choices = [('temperature', 'Temperature'), ('humidity', 'Humidity'), ('brightness', 'Brightness')])  
   operator = SelectField('', choices = [('<', '<'), ('>', '>'), ('=', '='), ('<=', '<='), ('>=','>=')])
   nombre = IntegerField("")
   number = IntegerField("Number:")
   periode = IntegerField("Periode:")
   text = TextAreaField("")

class UserForm(FlaskForm):
    username = TextField("UserName", validators=[InputRequired()])
    password = PasswordField("Password", validators=[InputRequired()])
    email    = TextField("Email Address", validators=[InputRequired()])
 

##################Added Code#####################
# Database configuration

global current_user
current_user = 'unknown'

client = MongoClient('localhost:27017')
db = client.capteurs # the database name is capteurs

@app.after_request
def add_header(res):
    res.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
    res.headers["Pragma"] = "no-cache"
    res.headers["Expires"] = "0"
    res.headers['Cache-Control'] = 'public, max-age=0'
    return res

@app.route('/index')
def index():
    return send_file('public/index.html')

# charts page
@app.route('/charts')
def charts():
    return send_file('public/charts.html')

# requests page
@app.route('/requests')
def requests():
    return send_file('public/requests.html')

@app.route('/public/<path:filename>')
def serve_static(filename):
    #root_dir = os.path.dirname(os.getcwd())
    #print(os.path.join('.','public/'),filename)
    return send_from_directory(os.path.join('.','public/'),filename)


  # Call this to save your data
@app.route('/save/<int:node_id>/<int:temperature>/<int:humidity>/<int:seq>')
def saveData(node_id, temperature, humidity, seq):
    try:
        db.data.insert_one({'date':time.strftime("%Y-%m-%d %H:%M:%S") ,'node':node_id, 'temperature': temperature, 'humidity':humidity, 'seq':seq})
        socketio.emit('newData', {'data': 'New Data'})
        return jsonify(status='OK',message='inserted successfully')
    except e:
        return jsonify(status='ERROR', message=str(e))
  # Call this to retreive your data
@app.route('/getData')
def getData():
    try:
        data = db.data.find({}).sort('$natural',-1).limit(11)
        return dumps(data)
    except e:
        return jsonify(status='ERROR', message=str(e))

# Call this to get all Nodes
@app.route('/getNodes')
def getNodes():
    #print(db.data.find().distinct('node'))
    return jsonify(db.data.find().distinct('node'))

# Call this to get a specific node data
@app.route('/getNodeData/<int:id>')
def getNodeData(id):
    #print(db.data.find({'node':id}))
    return dumps(db.data.find({'node':id}).sort('$natural',-1).limit(10))

##################End Added Code###################
@app.route('/', methods=['GET', 'POST'])
def login():
    error = None
    global current_user

    
    if request.method == 'POST':
        if request.form['username'] != 'admin' or request.form['password'] != 'admin':
            if checkUserExist(request.form['username'], request.form['password']) == False:
                error = 'Invalid Credentials. Please try again.'
            else:
                print "===============>: ",current_user
                return redirect(url_for('form'))
        else:
            current_user = 'admin'
            print "===============>: ",current_user
            return redirect(url_for('form'))
    return render_template('login.html', error=error)
  

        
@app.route('/form', methods=['GET', 'POST'])
def form():
    saveRequest(request)

    form = QueryForm()
   
    check=""
  
    if form.validate_on_submit():
       
       for i in form.selectlist.data:
          check+= str(i)+" "
   
       form.text.data+='select {}for {} by {} where {} {} {}'.format(check, form.number.data, form.periode.data,form.attribute.data, form.operator.data , form.nombre.data)
       msg=yacc.parse(str(form.text.data))
       UDP_IP= format(form.ip.data)
       UDP_PORT= int(form.port.data)
       print "UDP target IP:", UDP_IP
       print "UDP target port:", UDP_PORT
       print "message:", msg 
      
       sock = socket.socket(socket.AF_INET6, socket.SOCK_DGRAM)
       sock.bind(('aaaa::1', 3001,0,0)) 
       sock.sendto(msg, (UDP_IP, UDP_PORT))
       sock = socket.socket(socket.AF_INET6,socket.SOCK_DGRAM) # UDP
       sock.bind(('aaaa::1', 3001,0,0))
       # buffer size is 1024 bytes
       while True:
         w, addr = sock.recvfrom(1024)
         matchObj = re.match( 'node: (.*) t: (.*) l: (.*) seq: (.*) ', w)
         node_id     = matchObj.group(1)
         temperature = matchObj.group(2)
         humidity    = matchObj.group(3)
         seq         = matchObj.group(4)
         saveData(int(node_id), int(temperature), int(humidity), int(seq))
       
         print ("Data sent successfully")
     
       return render_template('form.html',form=form)
       
    return render_template('form.html',form=form)

@app.route('/getRequests')
def getRequests():
    try:
        data = db.requests.find({}).sort('$natural',-1)
        #data = db.requests.find({}).sort('$natural',-1).limit(10)
        return dumps(data)
    except e:
        return jsonify(status='ERROR', message=str(e))

def saveRequest(request):
    global current_user
    try:
        db.requests.insert_one({
            'username'  : current_user,
            'date'      :time.strftime("%Y-%m-%d %H:%M:%S"),
            'attribute' : request.form.getlist('attribute'),
            'periode'   : request.form.getlist('periode'),
            'ip'        : request.form.getlist('ip'),
            'selectlist': request.form.getlist('selectlist'),
            'number'    : request.form.getlist('number'),
            'text'      : request.form.getlist('text'),
            'operator'  : request.form.getlist('operator'),
            'nombre'    : request.form.getlist('nombre'),
            'port'      : request.form.getlist('port')})

        socketio.emit('newRequest', {'data': 'New Request'})
                             
    except Exception as e:
        print 'Exception: ', e

@app.route('/users')
def users():
    global current_user
    if current_user != 'admin':
        return send_file('public/403.html')
    return send_file('public/usersList.html')

@app.route('/user')
def user():
    form = UserForm()
    return render_template('user.html', form=form)

@app.route('/addUser', methods=['POST'])
def addUser():
    db.users.insert_one({
        'username' : request.form.getlist('username'),
        'password' : request.form.getlist('password'),
        'email'    : request.form.getlist('email')
    })
    form = QueryForm()
    return redirect('http://127.0.0.1:5000')

@app.route('/getUsers')
def getUsers():
    try:
        data = db.users.find({}).sort('$natural',-1)
        return dumps(data)
    except e:
        return jsonify(status='ERROR', message=str(e))

def checkUserExist(username, password):
    global current_user
    data = db.users.find_one({'username': username, 'password': password})
    if data == None:
        current_user = "unknown"
        return False
    current_user = username
    return True

@app.route('/deleteUser/<string:id>')
def deleteUser(id):
    global current_user

    if current_user == "admin":
        db.users.delete_one({'_id':ObjectId(id)})
        return send_file('templates/usersList.html')
    return send_file('public/403.html')

if __name__ == '__main__':
    app.debug = True
    socketio.run(app)
    #app.run(debug=True)
