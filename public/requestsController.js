var my_app = angular.module('my_app',[]);

my_app.factory('socket', function($rootScope){
  var socket = io.connect('http://127.0.0.1:5000');
  
  return {
    on: function(eventName, callback){
      socket.on(eventName, function(){
        var args = arguments
        $rootScope.$apply(function(){
          callback.apply(socket, args)
        })
      })
    }
  }
})

my_app.controller('rqstCtrl', function($scope, $http, socket){

$scope.model = []

$scope.getRequests = function(){
    $http.get("http://127.0.0.1:5000/getRequests")
    .then(response =>{
        $scope.model = response.data
        console.log($scope.model)
    })
}
socket.on('newRequest',function(data){
  $scope.getRequests()
})

$scope.getRequests()

})