var my_app = angular.module('my_app',['zingchart-angularjs']);


my_app.controller('LineCtrl', function($scope,$http,$interval){
  $scope.dataType = "temperature"
  $scope.initiale_Graph = function(){
      $scope.responseData = {}
      $scope.nodes=[]
      $scope.myData = {
      gui: {
        contextMenu: {
          button: {
            visible: 0
          }
        }
      },
      backgroundColor: "#434343",
      globals: {
          shadow: false,
          fontFamily: "Helvetica"
      },
      type: "area",

      legend: {
          layout: "x4",
          backgroundColor: "transparent",
          borderColor: "transparent",
          marker: {
              borderRadius: "50px",
              borderColor: "transparent"
          },
          item: {
              fontColor: "white"
          }

      },
      scaleX: {
          maxItems: 10,
          zooming: true,
          values: [],
          lineColor: "white",
          lineWidth: "1px",
          tick: {
              lineColor: "white",
              lineWidth: "1px"
          },
          item: {
              fontColor: "white"
          },
          guide: {
              visible: false
          }
      },
      scaleY: {
          lineColor: "white",
          lineWidth: "1px",
          tick: {
              lineColor: "white",
              lineWidth: "1px"
          },
          guide: {
              lineStyle: "solid",
              lineColor: "#626262"
          },
          item: {
              fontColor: "white"
          },
      },
      tooltip: {
          visible: false
      },
      crosshairX: {
          scaleLabel: {
              backgroundColor: "#fff",
              fontColor: "black"
          },
          plotLabel: {
              backgroundColor: "#434343",
              fontColor: "#FFF",
              _text: "Number of hits : %v"
          }
      },
      plot: {
          lineWidth: "2px",
          aspect: "spline",
          marker: {
              visible: false
          }
      },
      series: []
    }; 
  }
// Functions developped for zingChart  Begin
const getNodes = function(){

    $http.get("http://127.0.0.1:5000/getNodes")
    .then(response=>{
        //console.log(response.data)
        $scope.nodes = response.data
        $scope.nodes.map(node_id=>{
            //console.log("Getting data for node "+node_id)
            getNodeData(node_id)
        })
    })
    .catch(err=>{
        console.log(err)
    })
}

const getNodeData = function(node_id){
    let node_name = 'node'+node_id
    $http.get("http://127.0.0.1:5000/getNodeData/"+node_id)
    .then(response=>{
        //console.log(response.data)
        $scope.responseData[node_name] = response.data.reverse()
        //console.log($scope.responseData)
        fillGraphWithData(node_id, response.data.reverse())
    })
    .catch(err=>{
        console.log(err)
    })
}

const fillGraphWithData = function(node_id, data){
    let graphData = {text:"node"+node_id, values:[]}
    data.map(value=>{
        if($scope.dataType == "temperature")
            graphData.values.push(value.temperature)
        else
            graphData.values.push(value.humidity)
    })
    $scope.myData.series.push(graphData)
    //console.log($scope.myData)
}

$scope.refresh_zingChart = function(){
    $scope.initiale_Graph()
    getNodes()
}
// end zingChart functions


// Main code
//getData()
// call getData function every 500 milliseconds in order to refresh data
//$interval(getData, 1000)
$scope.initiale_Graph()
getNodes()

});
