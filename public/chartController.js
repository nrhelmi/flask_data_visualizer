var my_app = angular.module('my_app',['chart.js']);

my_app.factory('socket', function($rootScope){
  var socket = io.connect('http://127.0.0.1:5000');
  
  return {
    on: function(eventName, callback){
      socket.on(eventName, function(){
        var args = arguments
        $rootScope.$apply(function(){
          callback.apply(socket, args)
        })
      })
    }
  }
})

my_app.controller('LineCtrl', function($scope, $http, $interval, socket){
  
// function developped for chartjs
const test_chartjs = function(){
    $scope.labels = ["January", "February", "March", "April", "May", "June", "July"];
  $scope.series = [];
  $scope.data = [
    [65, 59, 80, 81, 56, 55, 40],
    [28, 48, 40, 19, 86, 27, 90]
  ];
  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }, { yAxisID: 'y-axis-2' }];
  $scope.options = {
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        },
        {
          id: 'y-axis-2',
          type: 'linear',
          display: true,
          position: 'right'
        }
      ]
    }
  };
}

  $scope.labels = [];
  $scope.series = [];
  $scope.data = [];
const drawChartjs = function(){

  $scope.onClick = function (points, evt) {
    console.log(points, evt);
  };
  $scope.datasetOverride = [{ yAxisID: 'y-axis-1' }];
  $scope.options = {animation:false,
    scales: {
      yAxes: [
        {
          id: 'y-axis-1',
          type: 'linear',
          display: true,
          position: 'left'
        }
      ]
    }
  };
}
$scope.nodes = []
$scope.dataType = "temperature"
$scope.nodeData = []
$scope.getNodes = function(){
    $http.get("http://127.0.0.1:5000/getNodes")
    .then(response=>{
        //console.log(response.data)
        $scope.nodes = response.data
        getNodeData($scope.selected_node)
    })
    .catch(err=>{
        console.log(err)
    })
}

const getNodeData = function(node_id){
    let node_name = 'node'+node_id
    $http.get("http://127.0.0.1:5000/getNodeData/"+node_id)
    .then(response=>{
        $scope.nodeData = response.data.reverse()
        //console.log($scope.nodeData)
        fillGraphWithData($scope.nodeData)
    })
    .catch(err=>{
        console.log(err)
    })
}
const fillGraphWithData = function(data){
    let array = {date:[], metrics:[]}
    data.map(value=>{
        //console.log(value)
        array.date.push(value.date)
        if($scope.dataType == "temperature")
            array.metrics.push(value.temperature)
        else 
            array.metrics.push(value.humidity)    
    })
    $scope.labels=array.date
    $scope.data=array.metrics
    drawChartjs()
}
// end chartjs functions

// Main code


//test_chartjs()
$scope.selected_node = "1"
$scope.getNodes()
//$interval(getNodes,800)
socket.on('newData',function(data){
  //console.log('Hello calling socket')
  //console.log(data)
  $scope.getNodes()
})

});
